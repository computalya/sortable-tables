module UsersHelper
  def sortable(column, title = nil)
    title ||= column.titleize

    # set arrow ASCII codes
      if column == sort_column
        arrow = sort_direction == 'asc' ? "&Darr;" : "&Uarr;"
      end
      
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"

    link_to "#{title}#{arrow}".html_safe, sort: column, direction: direction
  end
end
