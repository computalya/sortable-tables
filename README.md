# README

## screenshot

![sortable_tables](./sortable_tables.gif)

## create a demoapp

```bash
rails new sortable-tables -B -T
cd sortable-tables

bundle install --local

rails g scaffold User name lastname email
rails db:migrate
```

> config/routes.rb

```ruby
Rails.application.routes.draw do
  resources :users

  root 'users#index'
end
```

> db/seeds.rb

```ruby
User.create([
  { name: 'Kamil', lastname: 'Çağlar', email: 'kamil@caglar.com' },
  { name: 'Armağan', lastname: 'Uyduruk', email: 'arma@gmail.com' },
  { name: 'Emre', lastname: 'Zonguldak', email: 'emrez@hotmail.com' }
])
```

`rails db:reset`

## make columns sortable

just sort through controller

> app/view/users/index.html.erb

```html
  <thead>
    <tr>
      <th><%= link_to 'Name', sort: 'name' %></th>
      <th><%= link_to 'Lastname', sort: 'lastname' %></th> 
      <th><%= link_to 'Email', sort: 'email' %></th> 
      <th colspan="3"></th>
    </tr>
  </thead>
```

> app/controllers/users_controller.rb

```ruby
  def index
    @users = User.order(params[:sort])
  end
```

## use helper method

> app/view/users/index.html.erb

```html
  <thead>
    <tr>
      <th><%= sortable 'name'  %></th>
      <th><%= sortable 'lastname'  %></th> 
      <th><%= sortable 'email', 'e-mail' %></th> 
      <th colspan="3"></th>
    </tr>
  </thead>
```

> app/helpers/users_helper.rb

```ruby
module UsersHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"

    link_to title, sort: column, direction: direction
  end
end
```

> app/controllers/users_controller.rb

```ruby
  helper_method :sort_column, :sort_direction

  def index
    @users = User.order(sort_column + " " + sort_direction)
  end

  private
    def sort_column
      # allow only available column names in params[:sort]
      User.column_names.include?(params[:sort]) ? params[:sort] : "name"
    end

    def sort_direction
      # allow only [asc desc] in params[:direction]
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end  
```

## add arrows

> app/helpers/users_helper.rb

```ruby
module UsersHelper
  def sortable(column, title = nil)
    title ||= column.titleize

    # set arrow ASCII codes
      if column == sort_column
        arrow = sort_direction == 'asc' ? "&Darr;" : "&Uarr;"
      end
      
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"

    link_to "#{title}#{arrow}".html_safe, sort: column, direction: direction
  end
end

```

